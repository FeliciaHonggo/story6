from django.test import TestCase, Client
from django.urls import resolve
from .views import story7
import unittest


# Create your tests here.
class Story7UnitTest(TestCase):
	def test_url(self):
		c = Client()
		response = c.get('/story7/')
		self.assertEqual(response.status_code, 200)

	def test_template(self):
		c = Client()
		response = c.get('/story7/')
		self.assertTemplateUsed(response, 'story7.html')

	def test_func(self):
		found = resolve('/story7/')
		self.assertEqual(found.func, story7)

		