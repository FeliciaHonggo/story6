from django.test import TestCase, Client
from django.urls import resolve
from .views import story8
import unittest


# Create your tests here.
class Story8UnitTest(TestCase):
	def test_url(self):
		c = Client()
		response = c.get('/story8/')
		self.assertEqual(response.status_code, 200)

	def test_template(self):
		c = Client()
		response = c.get('/story8/')
		self.assertTemplateUsed(response, 'story8.html')

	def test_func(self):
		found = resolve('/story8/')
		self.assertEqual(found.func, story8)