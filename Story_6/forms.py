from django import forms
from .models import Status
from django.forms import widgets
from django.utils import timezone
import datetime

class statusForm(forms.Form):
	status = forms.CharField(widget = forms.TextInput(attrs={"id":"status"}))