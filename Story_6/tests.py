from django.test import TestCase, Client , LiveServerTestCase
from .views import index
from .models import Status
from selenium import webdriver
import unittest
import time
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


# Create your tests here.
class Story6UnitTest(TestCase):
	def test_url(self):
		c = Client()
		response = c.get('')
		self.assertEqual(response.status_code, 200)

	def test_template(self):
		c = Client()
		response = c.get('')
		self.assertTemplateUsed(response, 'index.html')

	def test_create_new_status(self):
		Status.objects.create(status='Coba Coba')
		counting = Status.objects.all().count()
		self.assertEqual(counting, 1)

class Story6FunctionalTest(LiveServerTestCase):
	def setUp(self):
		super().setUp()
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('--disable-gpu')
		self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)

	def tearDown(self):
		#self.selenium.implicitly_wait(10)
		self.selenium.quit()
		super().tearDown()

	def test_input(self):
		self.selenium.get(self.live_server_url)
		time.sleep(2)
		#print(self.selenium.page_source)
		isi = self.selenium.find_element_by_id('status')
		submit = self.selenium.find_element_by_id('submit')
		time.sleep(2)
		isi.send_keys('coba-coba wkwk')
		submit.send_keys(Keys.RETURN)
		time.sleep(2)
		self.assertIn('coba-coba wkwk', self.selenium.page_source)

