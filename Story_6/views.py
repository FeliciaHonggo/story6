from django.shortcuts import render, redirect
from .forms import statusForm
from .models import Status
import datetime

# Create your views here.
def index(request):
	if request.method == 'POST':
		form = statusForm(request.POST)
		if form.is_valid():
			status = form.cleaned_data['status']
			Status.objects.create(status=status)
			return redirect('Story_6:index')
	else :
		form = statusForm()
		hasil = Status.objects.all()
	return render(request, 'index.html', {'statusForm' : form, 'status':hasil})

def delete_status(request, pk):
	delete_status = Status.objects.get(pk=pk)
	delete_status.delete()
	return redirect('Story_6:index')
