from django.db import models
from django.utils import timezone
import datetime

# Create your models here.
class Status(models.Model):
	status = models.CharField(max_length=300, blank=False, null=False)
	date = models.DateTimeField(auto_now_add=True)