from django.urls import path
from . import views

app_name = 'Story_6'

urlpatterns = [
    path('', views.index, name='index'),
    path('delete/<pk>', views.delete_status, name="delete"),
   
    # dilanjutkan ...
]