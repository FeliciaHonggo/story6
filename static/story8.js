$(document).ready(() => {
    $.ajax({
        method: 'GET',
        url : 'https://www.googleapis.com/books/v1/volumes?q=random',
        success: function(response) {
            $('tbody').empty();
            for(let i=0; i < response.items.length; i++) {
                var row = document.createElement('tr');
                $(row).append('<th scope="row">' + (i+1) + '</th>');
                try {
                    $(row).append('<td class="cover"><img src="' + response.items[i].volumeInfo.imageLinks.thumbnail + '"></td>');
                }
                catch(e) {
                    $(row).append('<td class="cover">No image available</td>')
                }
                $(row).append('<td class="title">' + response.items[i].volumeInfo.title + '</td>');
                try {
                    $(row).append('<td class="deskripsi">' + response.items[i].volumeInfo.description + '</td>');
                }
                catch {
                    $(row).append('<td class="deskripsi">No description available</td>');
                }
                $(row).append('<td class="author">' + response.items[i].volumeInfo.authors + '</td>')
                try {
                    $(row).append('<td class="publisher">' + response.items[i].volumeInfo.publisher + '</td>');
                }
                catch {
                    $(row).append('<td class="publisher">We didnt know the publisher of this book</td>');
                }                

                $('tbody').append(row);
                console.log("testDone");
            }

        }
    })


    $('#button').click(function() {
        let key  = $('#myInput').val();
        $.ajax({
            method: 'GET',
            url : 'https://www.googleapis.com/books/v1/volumes?q=' + key,
            success: function(response) {
                $('tbody').empty();
                for(let i=0; i < response.items.length; i++) {
                    var row = document.createElement('tr');
                    $(row).append('<th scope="row">' + (i+1) + '</th>');
                try {
                    $(row).append('<td class="cover"><img src="' + response.items[i].volumeInfo.imageLinks.thumbnail + '"></td>');
                }
                catch(e) {
                    $(row).append('<td class="cover">No image available</td>')
                }
                $(row).append('<td class="title">' + response.items[i].volumeInfo.title + '</td>');
                try {
                    $(row).append('<td class="deskripsi">' + response.items[i].volumeInfo.description + '</td>');
                }
                catch {
                    $(row).append('<td class="deskripsi">No description available</td>');
                }
                $(row).append('<td class="author">' + response.items[i].volumeInfo.authors + '</td>')
                try {
                    $(row).append('<td class="publisher">' + response.items[i].volumeInfo.publisher + '</td>');
                }
                catch {
                    $(row).append('<td class="publisher">We didnt know the publisher of this book</td>');
                }
                    $('tbody').append(row);
                }
            }
        })
    })
})


