$(function(){
	$('.toggle').click(function () {
		theme_controlled = $('.day, .night');
		theme_controlled.toggleClass('day');
		theme_controlled.toggleClass('night');
	});

	var acc = $(".accordion");
	var i;

	for(i=0; i<acc.length;i++){
		acc[i].addEventListener("click", function(){
			this.classList.toggle("active");

			var panel = this.nextElementSibling;
			if(panel.style.display === "block") {
				panel.style.display = "none";
			}
			else {
				panel.style.display = "block";
			}
		})
	};
})
